window.addEventListener('resize', adjustVh());

function adjustVh(){
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
}

export {adjustVh};
