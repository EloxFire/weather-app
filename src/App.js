import React, {useState, useEffect} from 'react';
import Setting from './components/Setting';
import {adjustVh} from './js/adjust.js';
import {API_KEY} from './config.js';
import './main.scss';

function App() {

  const [weather, setWeather] = useState([]);
  const [history, setHistory] = useState([]);
  const [searchedTown, setSearchedTown] = useState("");

  useEffect(() => {
    getWeather();
  }, []);


  setInterval(() => {
    getWeather();
  }, 60000);

  const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  const getWeather = () => {
    navigator.geolocation.getCurrentPosition((position) => {
      let lat = position.coords.latitude;
      let lng = position.coords.longitude;

      fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&units=metric&appid=${API_KEY}`)
      .then(response => response.json())
      .then(response => {
        setWeather(response);
      })
    }, () => {
      console.log("Geolocation Error");
    }, {
      enableHighAccuracy: true,
    });
  }

  const openMenu = () => {
    if(window.innerWidth < 992){
      document.getElementById('sidenav').style.width = "350px";
    }else if(window.innerWidth < 792){
      document.getElementById('sidenav').style.width = "290px";
    }else{
      document.getElementById('sidenav').style.width = "600px";
    }
  }
  const closeMenu = () => {
    document.getElementById('sidenav').style.width = "0";
  }

  const searchNewTown = (e) => {
    e.preventDefault();
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${searchedTown}&units=metric&appid=${API_KEY}`)
    .then(response => response.json())
    .then(response => {
      if(response.cod === 404){
        document.getElementById('error-message').style.color = '#e95656';
        document.getElementById('error-message').innerHTML = "Ville inconue...";
        setInterval(() => {
          document.getElementById('error-message').style.color = '#000';
          document.getElementById('error-message').innerHTML = "";
        }, 4000);
      }else if(response.cod !== 404 && response.cod !== 200){
        document.getElementById('error-message').style.color = '#e95656';
        document.getElementById('error-message').innerHTML = "Une erreur est survenue...";
        setInterval(() => {
          document.getElementById('error-message').style.color = '#000';
          document.getElementById('error-message').innerHTML = "";
        }, 4000);
      }else{
        setWeather(response);
        setHistory([...history, capitalizeFirstLetter(searchedTown)]);
      }
    })
  }

  const convertToDirection = (degrees) => {
    let directions = ['Nord', 'Nord/Est', 'Est', 'Sud/Est', 'Sud', 'Sud/Ouest', 'Ouest', 'Nord/Ouest'];
    // Split into the 8 directions
    degrees = degrees * 8 / 360;
    // round to nearest integer.
    degrees = Math.round(degrees, 0);
    // Ensure it's within 0-7
    degrees = (degrees + 8) % 8

    return directions[degrees];
  }

  adjustVh();
  return (
    <div id="app" className="d-flex flex-column flex-lg-row">

      <img id="menuButton" src="./img/logo/menu.png" alt="Open menu button" onClick={() => openMenu()}/>
      <div id="sidenav" className="sidenav d-flex flex-column justify-content-center">
        <p className="closebtn" onClick={() => closeMenu()}>&times;</p>
        <form onSubmit={searchNewTown} className="ms-5 col-7 mb-5">
          <div className="mb-3 d-flex flex-column">
            <label for="town" className="form-label">Rechercher une autre ville</label>
            <input type="text" id="town" placeholder="Nom de la ville" onChange={(e) => setSearchedTown(e.target.value)}/>
          </div>
          <button type="submit" className="searchButton">Trouver</button>
          <p id="error-message"></p>
        </form>
        <div className="history col-7 ms-5 mt-5">
          <p>Historique de recherches :</p>
          <div className="historyLog">
            {
              history.length === 0 ?
                <p className="historyChoice">Faites une recherche pour qu'elle apparaisse dans votre historique</p>
              :
              history.map((town, index) => {
                return(
                  <p className="m-0 historyChoice">{town}</p>
                )
              })
            }
          </div>
        </div>
        <div className="col-12 text-center mt-5">
          <p>Vous avez recherché {history.length} villes</p>
        </div>
      </div>

      <div className="global-pane col-12 col-lg-4 d-flex flex-column align-items-center justify-content-center">
        <div className="currentTemp text-center mb-4">
          <p className="p-0 m-0 currentTempText">{weather.length !== 0 ? `${weather.main.temp.toFixed(0)}°` : ".."}</p>
          <p className="p-0 m-0 currentTempTown">{weather.length !== 0 ? weather.name : ".."}</p>
          {/* <button onClick={() => getWeather()}>GET WEATHER</button> */}
        </div>
        <div className="currentWeather text-center mt-5">
          <img className="currentWeatherLogo" src="./img/logo/sun.png" alt="Weather icon"/>
          <p className="currentWeatherDescription">{weather.length !== 0 ? capitalizeFirstLetter(weather.weather[0].description) : ".."}</p>
        </div>
      </div>


      <div className="detail-pane col-12 col-lg-8 d-flex flex-column justify-content-start justify-content-lg-center">
        <div>
          <p className="sectionTitle">Details :</p>
          <div className="d-flex flex-column flex-lg-row justify-content-lg-between col-10 mb-5">
            <div>
              <p className="conditions-text">Pression : {weather.length !== 0 ? `${weather.main.pressure} hPa` : ".."}</p>
              <p className="conditions-text">Humidité : {weather.length !== 0 ? `${weather.main.humidity}%` : ".."}</p>
              <p className="conditions-text">Visibilité : {weather.length !== 0 ? `${weather.visibility / 1000} Km ` : ".."}</p>
            </div>
            <div>
              <p className="conditions-text">Vent : {weather.length !== 0 ? `${weather.wind.speed} m/s` : ".."}</p>
              <p className="conditions-text">Direction : {weather.length !== 0 ? convertToDirection(weather.wind.deg) : ".."}</p>
              <p className="conditions-text">Nuages : {weather.length !== 0 ? `${weather.clouds.all}%` : ".."}</p>
            </div>
          </div>

          <p className="sectionTitle mt-5">Environment :</p>
          <div className="d-flex flex-column flex-lg-row justify-content-lg-between col-10 mb-5 mb-lg-0">
            <div>
              <p className="conditions-text">Max : {weather.length !== 0 ? `${weather.main.temp_max.toFixed(0)}°` : ".."}</p>
              <p className="conditions-text">Min : {weather.length !== 0 ? `${weather.main.temp_min.toFixed(0)}°` : ".."}</p>
            </div>
            <div>
              <p className="conditions-text">Levé soleil : {weather.length !== 0 ? new Date(weather.sys.sunrise * 1000).toLocaleTimeString('fr-FR').toString() : ".."}</p>
              <p className="conditions-text">Coucher soleil : {weather.length !== 0 ? new Date(weather.sys.sunset * 1000).toLocaleTimeString('fr-FR').toString() : ".."}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
