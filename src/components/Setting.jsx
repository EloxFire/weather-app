import React from 'react';
import '../sass/setting.scss';

function Setting(props){
  return(
    <div className="setting d-flex flex-column align-items-center p-3">
      <p className="title">{props.title}</p>
      <p className="value">{props.value}</p>
    </div>
  )
}

export default Setting;
